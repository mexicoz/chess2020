﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Chessman : NetworkBehaviour {
    //References to objects in our Unity Scene
    public GameObject controller;
    public GameObject movePlate;
    GameObject obj;

    //Position for this Chesspiece on the Board
    //The correct position will be set later
    private int xBoard = -1;
    private int yBoard = -1;

    //Variable for keeping track of the player it belongs to "black" or "white"
    private string player;

    //References to all the possible Sprites that this Chesspiece could be
    public Sprite black_queen, black_knight, black_bishop, black_king, black_rook, black_pawn, black_pawn2, black_dragon;
    public Sprite white_queen, white_knight, white_bishop, white_king, white_rook, white_pawn, white_pawn2, white_dragon;

    public bool checkMate = false;

    [ServerCallback]
    public void Activate() {
        //Get the game controller
        controller = GameObject.FindGameObjectWithTag("GameController");

        //Take the instantiated location and adjust transform
        SetCoords();

        //Choose correct sprite based on piece's name
        switch (this.name) {
            case "black_queen": this.GetComponent<SpriteRenderer>().sprite = black_queen; player = "black"; break;
            case "black_knight": this.GetComponent<SpriteRenderer>().sprite = black_knight; player = "black"; break;
            case "black_bishop": this.GetComponent<SpriteRenderer>().sprite = black_bishop; player = "black"; break;
            case "black_king": this.GetComponent<SpriteRenderer>().sprite = black_king; player = "black"; break;
            case "black_rook": this.GetComponent<SpriteRenderer>().sprite = black_rook; player = "black"; break;
            case "black_pawn": this.GetComponent<SpriteRenderer>().sprite = black_pawn; player = "black"; break;
            case "black_pawn2": this.GetComponent<SpriteRenderer>().sprite = black_pawn2; player = "black"; break;
            case "black_dragon": this.GetComponent<SpriteRenderer>().sprite = black_dragon; player = "black"; break;
            case "white_queen": this.GetComponent<SpriteRenderer>().sprite = white_queen; player = "white"; break;
            case "white_knight": this.GetComponent<SpriteRenderer>().sprite = white_knight; player = "white"; break;
            case "white_bishop": this.GetComponent<SpriteRenderer>().sprite = white_bishop; player = "white"; break;
            case "white_king": this.GetComponent<SpriteRenderer>().sprite = white_king; player = "white"; break;
            case "white_rook": this.GetComponent<SpriteRenderer>().sprite = white_rook; player = "white"; break;
            case "white_pawn": this.GetComponent<SpriteRenderer>().sprite = white_pawn; player = "white"; break;
            case "white_pawn2": this.GetComponent<SpriteRenderer>().sprite = white_pawn2; player = "white"; break;
            case "white_dragon": this.GetComponent<SpriteRenderer>().sprite = white_dragon; player = "white"; break;
        }

        NetworkServer.Spawn(gameObject);
    }
    
    public void SetCoords() {
        //Get the board value in order to convert to xy coords
        float x = xBoard;
        float y = yBoard;

        //Adjust by variable offset
        x *= 4f;
        y *= 4f;

        //Add constants (pos 0,0)
        x += -18f;
        y += -18f;

        //Set actual unity values
        this.transform.position = new Vector3(x, y, -1.0f);
    }

    public int GetXBoard() {
        return xBoard;
    }

    public int GetYBoard() {
        return yBoard;
    }

    public void SetXBoard(int x) {
        xBoard = x;
    }
    public void SetXBoardCastlingLeft(int x) {
        xBoard = x - 2;
    }
    public void SetXBoardCastlingRight(int x) {
        xBoard = x + 3;
    }
    public void SetYBoard(int y) {
        yBoard = y;
    }
    
    [ServerCallback]
    private void OnMouseUp() {
        if (!controller.GetComponent<Game>().IsGameOver() && controller.GetComponent<Game>().GetCurrentPlayer() == player) {
            //Remove all moveplates relating to previously selected piece
            DestroyMovePlates();

            //Create new MovePlates
            InitiateMovePlates();           
        }
    }

    public void DestroyMovePlates() {
        //Destroy old MovePlates
        GameObject[] movePlates = GameObject.FindGameObjectsWithTag("MovePlate");
        for (int i = 0; i < movePlates.Length; i++) {
            Destroy(movePlates[i]); //Be careful with this function "Destroy" it is asynchronous
        }
    }
    public void EnableMovePlates() {
        GameObject[] movePlates = GameObject.FindGameObjectsWithTag("MovePlate");
        for (int i = 0; i < movePlates.Length; i++) {
            movePlates[i].GetComponent<SpriteRenderer>().enabled = false;
            movePlates[i].GetComponent<BoxCollider2D>().enabled = false;
        }
    }
    public void InitiateMovePlates() {
        switch (this.name) {
            case "black_queen":
            case "white_queen":
                LineMovePlate(1, 0);
                LineMovePlate(0, 1);
                LineMovePlate(1, 1);
                LineMovePlate(-1, 0);
                LineMovePlate(0, -1);
                LineMovePlate(-1, -1);
                LineMovePlate(-1, 1);
                LineMovePlate(1, -1);
                break;
            case "black_dragon":
            case "white_dragon":
                DragonMovePlate();
                DragonMovePlate();
                DragonMovePlate();
                DragonMovePlate();
                DragonMovePlate();
                DragonMovePlate();
                DragonMovePlate();
                DragonMovePlate();
                break;
            case "black_knight":
            case "white_knight":
                LMovePlate();
                break;
            case "black_bishop":
            case "white_bishop":
                LineMovePlate(1, 1);
                LineMovePlate(1, -1);
                LineMovePlate(-1, 1);
                LineMovePlate(-1, -1);
                break;
            case "black_king":
            case "white_king":
                SurroundMovePlate();
                LineCastlingMovePlate(+ 1, 0);  // + 4
                LineCastlingMovePlate(- 1, 0);  // - 5
                break;
            case "black_rook":
            case "white_rook":
                LineMovePlate(1, 0);
                LineMovePlate(0, 1);
                LineMovePlate(-1, 0);
                LineMovePlate(0, -1);
                break;
            case "black_pawn":
                PawnMovePlate(xBoard, yBoard - 1);
                break;
            case "white_pawn":
                PawnMovePlate(xBoard, yBoard + 1);
                break;
            case "black_pawn2":
                PawnMovePlate2(xBoard, yBoard - 1);
                break;
            case "white_pawn2":
                PawnMovePlate2(xBoard, yBoard + 1);
                break;
        }
    }
    bool IsPawnMoveToTwoPlates(int xIncrement, int yIncrement) {
        Game sc = controller.GetComponent<Game>();

        int x = xBoard + xIncrement;
        int y = yBoard + yIncrement;
        if (sc.PositionOnBoard(x, y) && sc.GetPosition(x, y) == null)
            return true;
        else
            return false;
    }
    public void LineMovePlate(int xIncrement, int yIncrement) {
        Game sc = controller.GetComponent<Game>();
        

        int x = xBoard + xIncrement;
        int y = yBoard + yIncrement;

        
            while (sc.PositionOnBoard(x, y) && sc.GetPosition(x, y) == null) {
                MovePlateSpawn(x, y);
                x += xIncrement;
                y += yIncrement;
            }        

        if (sc.PositionOnBoard(x, y) && sc.GetPosition(x, y).GetComponent<Chessman>().player != player) {
            MovePlateAttackSpawn(x, y);
        }
    }    
    public void LineCastlingMovePlate(int xIncrement, int yIncrement) {
        if (!FindObjectOfType<Game>().check) {
            Game sc = controller.GetComponent<Game>();

            int x = xBoard + xIncrement;
            int y = yBoard + yIncrement;

            while (sc.PositionOnBoard(x, y) && sc.GetPosition(x, y) == null) {

                x += xIncrement;
                y += yIncrement;
            }           
            if (sc.PositionOnBoard(x, y) && sc.GetPosition(x, y).GetComponent<Chessman>().player == player && x == xBoard + 4) {
                MovePlateCastlingSpawn(x, y);
            }
            if (sc.PositionOnBoard(x, y) && sc.GetPosition(x, y).GetComponent<Chessman>().player == player && x == xBoard - 5) {
                MovePlateCastlingSpawn(x, y);
            }
           
            
        }
       
    }
    public void DragonMovePlate() {
        // PointMovePlate(xBoard, yBoard + 1);
        // PointMovePlate(xBoard, yBoard - 1);
        // PointMovePlate(xBoard - 1, yBoard + 0);
        // PointMovePlate(xBoard - 1, yBoard - 1);
        // PointMovePlate(xBoard - 1, yBoard + 1);
        // PointMovePlate(xBoard + 1, yBoard + 0);
        // PointMovePlate(xBoard + 1, yBoard - 1);
        // PointMovePlate(xBoard + 1, yBoard + 1);
        //
        // PointMovePlate(xBoard, yBoard + 2);
        // PointMovePlate(xBoard, yBoard - 2);
        // PointMovePlate(xBoard - 2, yBoard + 0);
        // PointMovePlate(xBoard - 2, yBoard - 2);
        // PointMovePlate(xBoard - 2, yBoard + 2);
        // PointMovePlate(xBoard + 2, yBoard + 0);
        // PointMovePlate(xBoard + 2, yBoard - 2);
        // PointMovePlate(xBoard + 2, yBoard + 2);
        PointMovePlate(xBoard + 1, yBoard + 3);
        PointMovePlate(xBoard - 1, yBoard + 3);
        PointMovePlate(xBoard + 3, yBoard + 1);
        PointMovePlate(xBoard + 3, yBoard - 1);
        PointMovePlate(xBoard + 1, yBoard - 3);
        PointMovePlate(xBoard - 1, yBoard - 3);
        PointMovePlate(xBoard - 3, yBoard + 1);
        PointMovePlate(xBoard - 3, yBoard - 1);
    }
    public void LMovePlate() {
        PointMovePlate(xBoard + 1, yBoard + 2);
        PointMovePlate(xBoard - 1, yBoard + 2);
        PointMovePlate(xBoard + 2, yBoard + 1);
        PointMovePlate(xBoard + 2, yBoard - 1);
        PointMovePlate(xBoard + 1, yBoard - 2);
        PointMovePlate(xBoard - 1, yBoard - 2);
        PointMovePlate(xBoard - 2, yBoard + 1);
        PointMovePlate(xBoard - 2, yBoard - 1);
    }

    public void SurroundMovePlate() {       
        PointMovePlateKing(xBoard, yBoard + 1);
        PointMovePlateKing(xBoard, yBoard - 1);
        PointMovePlateKing(xBoard - 1, yBoard + 0);
        PointMovePlateKing(xBoard - 1, yBoard - 1);
        PointMovePlateKing(xBoard - 1, yBoard + 1);
        PointMovePlateKing(xBoard + 1, yBoard + 0);
        PointMovePlateKing(xBoard + 1, yBoard - 1);
        PointMovePlateKing(xBoard + 1, yBoard + 1);
    }
   
   
    public void PointMovePlate(int x, int y) {
        Game sc = controller.GetComponent<Game>();

        if (sc.PositionOnBoard(x, y)) {
            GameObject cp = sc.GetPosition(x, y);

            if (cp == null) {
                MovePlateSpawn(x, y);                
            }
            else if (cp.GetComponent<Chessman>().player != player) {
                MovePlateAttackSpawn(x, y);
            }        
        }
    }
    public void PointMovePlateKing(int x, int y) {
        Game sc = controller.GetComponent<Game>();

        if (sc.PositionOnBoard(x, y)) {
            GameObject cp = sc.GetPosition(x, y);

            if (cp == null) {
                MovePlateSpawnKing(x, y);
            }
            else if (cp.GetComponent<Chessman>().player != player) {
                MovePlateAttackSpawnKing(x, y);
            }
        }
    }

    public void PawnMovePlate(int x, int y) {
       
        Game sc = controller.GetComponent<Game>();
       
        if (sc.PositionOnBoard(x, y)) {
            if (sc.GetPosition(x, y) == null) {
                MovePlateSpawn(x, y);
            }

            if (sc.PositionOnBoard(x + 1, y) && sc.GetPosition(x + 1, y) != null && sc.GetPosition(x + 1, y).GetComponent<Chessman>().player != player) {
                MovePlateAttackSpawn(x + 1, y);
            }
            if (sc.PositionOnBoard(x - 1, y) && sc.GetPosition(x - 1, y) != null && sc.GetPosition(x - 1, y).GetComponent<Chessman>().player != player) {
                MovePlateAttackSpawn(x - 1, y);
            }    

            if (IsPawnMoveToTwoPlates(0, -1) && yBoard > 7 && sc.GetPosition(x, y - 1) == null) {
                if (sc.GetPosition(x, y) == null) {
                    MovePlateSpawn(x, y - 1);
                }
            }
            if (IsPawnMoveToTwoPlates(0, 1) && yBoard < 2 && sc.GetPosition(x, y + 1) == null) {
                if (sc.GetPosition(x, y) == null) {
                    MovePlateSpawn(x, y + 1);
                }
            }
        }
    }
    public void PawnMovePlate2(int x, int y) {

        Game sc = controller.GetComponent<Game>();

        if (sc.PositionOnBoard(x, y)) {
            if (sc.GetPosition(x, y) == null) {
                MovePlateSpawn(x, y);
            }

            if (sc.PositionOnBoard(x + 1, y) && sc.GetPosition(x + 1, y) != null && sc.GetPosition(x + 1, y).GetComponent<Chessman>().player != player) {
                MovePlateAttackSpawn(x + 1, y);                
            }

            if (sc.PositionOnBoard(x - 1, y) && sc.GetPosition(x - 1, y) != null && sc.GetPosition(x - 1, y).GetComponent<Chessman>().player != player) {
                MovePlateAttackSpawn(x - 1, y);                
            }                      

            if (IsPawnMoveToTwoPlates(0, -1) && yBoard > 6 && sc.GetPosition(x, y - 1) == null) {
                if (sc.GetPosition(x, y) == null) {
                    MovePlateSpawn(x, y - 1);
                }
            }
            if (IsPawnMoveToTwoPlates(0, 1) && yBoard < 3 && sc.GetPosition(x, y + 1) == null) {
                if (sc.GetPosition(x, y) == null) {
                    MovePlateSpawn(x, y + 1);
                }
            }
        }
    }   

    public void MovePlateSpawn(int matrixX, int matrixY) {

        //Get the board value in order to convert to xy coords
        float x = matrixX;
        float y = matrixY;

        //Adjust by variable offset
        x *= 4f;  // 0.66f
        y *= 4f;

        //Add constants (pos 0,0)
        x += -18f;  // -2.3f
        y += -18f;

        //Set actual unity values
        GameObject mp = Instantiate(movePlate, new Vector3(x, y, -3.0f), Quaternion.identity);
                
        MovePlate mpScript = mp.GetComponent<MovePlate>();
        
        mpScript.SetReference(gameObject);
        mpScript.SetCoords(matrixX, matrixY);        
    }
    
    public void MovePlateAttackSpawn(int matrixX, int matrixY) {
        //Get the board value in order to convert to xy coords
        float x = matrixX;
        float y = matrixY;

        //Adjust by variable offset
        x *= 4f;
        y *= 4f;

        //Add constants (pos 0,0)
        x += -18f;
        y += -18f;

        //Set actual unity values
        GameObject mp = Instantiate(movePlate, new Vector3(x, y, -3.0f), Quaternion.identity);

        MovePlate mpScript = mp.GetComponent<MovePlate>();
        mpScript.attack = true;
        mpScript.check = true;        
        
        mpScript.SetReference(gameObject);
        mpScript.SetCoords(matrixX, matrixY);
    }
    public void MovePlateSpawnKing(int matrixX, int matrixY) {

        //Get the board value in order to convert to xy coords
        float x = matrixX;
        float y = matrixY;

        //Adjust by variable offset
        x *= 4f;  // 0.66f
        y *= 4f;

        //Add constants (pos 0,0)
        x += -18f;  // -2.3f
        y += -18f;

        //Set actual unity values
        GameObject mp = Instantiate(movePlate, new Vector3(x, y, -3.0f), Quaternion.identity);

        MovePlate mpScript = mp.GetComponent<MovePlate>();
        mpScript.check = true;
        mpScript.checkMate = true;

        mpScript.SetReference(gameObject);
        mpScript.SetCoords(matrixX, matrixY);
    }
    public void MovePlateAttackSpawnKing(int matrixX, int matrixY) {
        //Get the board value in order to convert to xy coords
        float x = matrixX;
        float y = matrixY;

        //Adjust by variable offset
        x *= 4f;
        y *= 4f;

        //Add constants (pos 0,0)
        x += -18f;
        y += -18f;

        //Set actual unity values
        GameObject mp = Instantiate(movePlate, new Vector3(x, y, -3.0f), Quaternion.identity);

        MovePlate mpScript = mp.GetComponent<MovePlate>();
        mpScript.attack = true;
        mpScript.check = true;
        mpScript.checkMate = true;
        
        mpScript.SetReference(gameObject);
        mpScript.SetCoords(matrixX, matrixY);
    }
    public void MovePlateCastlingSpawn(int matrixX, int matrixY) {
        Game sc = controller.GetComponent<Game>();
        //Get the board value in order to convert to xy coords
        float x = matrixX;
        float y = matrixY;

        //Adjust by variable offset
        x *= 4f;
        y *= 4f;

        //Add constants (pos 0,0)
        x += -18f;
        y += -18f;

        //Set actual unity values
        GameObject mp = Instantiate(movePlate, new Vector3(x, y, -3.0f), Quaternion.identity);

        MovePlate mpScript = mp.GetComponent<MovePlate>();
       
        mpScript.castling = true;

        mpScript.SetReference(gameObject);
        mpScript.SetCoords(matrixX, matrixY);
    }
   
}
