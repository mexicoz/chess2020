﻿
using Mirror;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Game : NetworkBehaviour {
    //Reference from Unity IDE
    public GameObject chesspiece;
    public int count;
    //Matrices needed, positions of each of the GameObjects
    //Also separate arrays for the players in order to easily keep track of them all
    //Keep in mind that the same objects are going to be in "positions" and "playerBlack"/"playerWhite"
    private GameObject[,] positions = new GameObject[10, 10];
    private GameObject[] playerBlack = new GameObject[20];
    private GameObject[] playerWhite = new GameObject[20];

    public GameObject[] buttonWhiteChanges;
    public GameObject[] buttonBlackChanges;

    public GameObject pawn = null;
    
    public GameObject checkImageWhite;
    public GameObject checkImageBlack;
   
    public Sprite checkImage;
    public Sprite checkMateImage;

    //current turn
    public string currentPlayer = "white";
    public string nameNewCheess; 

    //Game Ending
    private bool gameOver = false;
    public bool check = false;
    public bool isCheckout = false;
    public int pawnXposition;
    public int pawnYposition;

    public override void OnStartServer() {

        base.OnStartServer();

        playerWhite = new GameObject[] { Create("white_rook", 0, 0),

        Create("white_knight", 1, 0), Create("white_dragon", 2, 0), Create("white_bishop", 3, 0),

        Create("white_queen", 4, 0), Create("white_king", 5, 0),

        Create("white_bishop", 6, 0), Create("white_dragon", 7, 0), Create("white_knight", 8, 0),

        Create("white_rook", 9, 0),

        Create("white_pawn", 0, 1), Create("white_pawn", 1, 1), Create("white_pawn", 2, 1),
        Create("white_pawn", 3, 1), Create("white_pawn", 4, 1), Create("white_pawn", 5, 1),
        Create("white_pawn", 6, 1), Create("white_pawn", 7, 1), Create("white_pawn", 8, 1),
        Create("white_pawn", 9, 1),
        Create("white_pawn2", 0, 2), Create("white_pawn2", 1, 2), Create("white_pawn2", 2, 2),
        Create("white_pawn2", 3, 2), Create("white_pawn2", 4, 2), Create("white_pawn2", 5, 2),
        Create("white_pawn2", 6, 2), Create("white_pawn2", 7, 2), Create("white_pawn2", 8, 2),
        Create("white_pawn2", 9, 2)
        };        
        
        playerBlack = new GameObject[] { Create("black_rook", 0, 9),

        Create("black_knight",1,9), Create("black_dragon", 2, 9), Create("black_bishop",3, 9),

        Create("black_queen",4,9), Create("black_king",5,9),

        Create("black_bishop",6,9), Create("black_dragon", 7, 9), Create("black_knight",8,9),

        Create("black_rook",9,9),

        Create("black_pawn", 0, 8), Create("black_pawn", 1, 8), Create("black_pawn", 2, 8),
        Create("black_pawn", 3, 8), Create("black_pawn", 4, 8), Create("black_pawn", 5, 8),
        Create("black_pawn", 6, 8), Create("black_pawn", 7, 8), Create("black_pawn", 8, 8),
        Create("black_pawn", 9, 8),
        Create("black_pawn2", 0, 7), Create("black_pawn2", 1, 7), Create("black_pawn2", 2, 7),
        Create("black_pawn2", 3, 7), Create("black_pawn2", 4, 7), Create("black_pawn2", 5, 7),
        Create("black_pawn2", 6, 7), Create("black_pawn2", 7, 7), Create("black_pawn2", 8, 7),
        Create("black_pawn2", 9, 7)
        };

        
        //Set all piece positions on the positions board
        for (int i = 0; i < playerBlack.Length; i++) {
            SetPosition(playerBlack[i]);
            SetPosition(playerWhite[i]);            
        }
    }

    public GameObject Create(string name, int x, int y) {
        GameObject obj = Instantiate(chesspiece, new Vector3(0, 0, -1), Quaternion.identity);
        Chessman cm = obj.GetComponent<Chessman>(); //We have access to the GameObject, we need the script
        cm.name = name; //This is a built in variable that Unity has, so we did not have to declare it before
        cm.SetXBoard(x);
        cm.SetYBoard(y);
        cm.Activate(); //It has everything set up so it can now Activate()
        return obj;
    }
    
    //Set new chess when some pawn arrived the last position
    public void ChangePawn(string name) {        
        nameNewCheess = name;
        GameObject go = Create(nameNewCheess, pawnXposition, pawnYposition);
        SetPosition(go);
        if (pawn != null) {
            Destroy(pawn);
        }
    }    
    public void CheckOutKing() {
        
        if (currentPlayer == "black") {
            for (int i = 0; i < playerWhite.Length; i++) {
                if(playerWhite[i]!=null)
                   playerWhite[i].GetComponent<Chessman>().InitiateMovePlates();           
            }           
        }
       if (currentPlayer == "white") {            
            for (int i = 0; i < playerBlack.Length; i++) {
                if (playerBlack[i] != null)
                    playerBlack[i].GetComponent<Chessman>().InitiateMovePlates();
            }            
        }
    }   
    public void CheckOutKingMate() {
        if (currentPlayer == "black") {
            for (int i = 0; i < playerBlack.Length; i++) {
                if (playerBlack[i] != null) {
                    if (playerBlack[i].name == "black_king")
                        playerBlack[i].GetComponent<Chessman>().SurroundMovePlate();
                }
            }
        }
        if (currentPlayer == "white") {
            for (int i = 0; i < playerWhite.Length; i++) {
                if (playerWhite[i] != null) {
                    if (playerWhite[i].name == "white_king")
                        playerWhite[i].GetComponent<Chessman>().SurroundMovePlate();
                }
            }
        }
    }
    public void SetVisibleWhiteChangesPlate() {
        for (int i = 0; i < buttonWhiteChanges.Length; i++) {
            if (buttonWhiteChanges[i].activeSelf)
                buttonWhiteChanges[i].SetActive(false);
            else
                buttonWhiteChanges[i].SetActive(true);
        }
        
    }
    public void SetVisibleBlackChangesPlate() {
        for (int i = 0; i < buttonBlackChanges.Length; i++) {
            if (buttonBlackChanges[i].activeSelf)
                buttonBlackChanges[i].SetActive(false);
            else
                buttonBlackChanges[i].SetActive(true);
        }
    }
    public void SetPosition(GameObject obj) {
        Chessman cm = obj.GetComponent<Chessman>();

        //Overwrites either empty space or whatever was there
        positions[cm.GetXBoard(), cm.GetYBoard()] = obj;
    }

    public void SetPositionEmpty(int x, int y) {
        positions[x, y] = null;
    }

    public GameObject GetPosition(int x, int y) {
        return positions[x, y];
    }

    public bool PositionOnBoard(int x, int y) {
        if (x < 0 || y < 0 || x >= positions.GetLength(0) || y >= positions.GetLength(1)) return false;
        return true;
    }

    public string GetCurrentPlayer() {
        return currentPlayer;
    }

    public bool IsGameOver() {
        return gameOver;
    }

    public void NextTurn() {
        if (currentPlayer == "white") {
            currentPlayer = "black";
        }
        else {
            currentPlayer = "white";
        }
    }
    [ServerCallback]
    public void Update() {
        if (gameOver == true && Input.GetMouseButtonDown(0)) {
            gameOver = false;

            //Using UnityEngine.SceneManagement is needed here
            SceneManager.LoadScene("Game"); //Restarts the game by loading the scene over again
        }
    }
    public void Winner(string playerWinner) {
        gameOver = true;

        //Using UnityEngine.UI is needed here
        GameObject.FindGameObjectWithTag("WinnerText").GetComponent<Text>().enabled = true;
        GameObject.FindGameObjectWithTag("WinnerText").GetComponent<Text>().text = playerWinner + " is the winner";

        GameObject.FindGameObjectWithTag("RestartText").GetComponent<Text>().enabled = true;
    }
   
}
