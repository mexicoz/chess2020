﻿
using Mirror;
using UnityEngine;
using UnityEngine.UI;

public class MovePlate : NetworkBehaviour {
    //Some functions will need reference to the controller
    public GameObject controller;

    //The Chesspiece that was tapped to create this MovePlate
    GameObject reference = null;

    //Location on the board
    int matrixX;
    int matrixY;

    //false: movement, true: attacking
    public bool attack = false;
    public bool castling = false;
    public bool check = false; 
    public bool checkMate = true;

    private bool gameover = false;


    public void Start() {

        Chek();

        //if (attack) {
        //    //Set to red
        //    gameObject.GetComponent<SpriteRenderer>().color = new Color(1.0f, 0.0f, 0.0f, 1.0f);
        //}
        //if (castling) {
        //    gameObject.GetComponent<SpriteRenderer>().color = new Color(0.0f, 1.0f, 0.0f, 1.0f);
        //}
    }
    public void Chek() {
        controller = GameObject.FindGameObjectWithTag("GameController");
        Game game = FindObjectOfType<Game>();
       

        if (check) {
            GameObject cp = controller.GetComponent<Game>().GetPosition(matrixX, matrixY);

            if (!checkMate) {
                if (cp.name == "white_king") {                    
                    game.checkImageWhite.GetComponent<SpriteRenderer>().enabled = true;
                    game.checkImageWhite.GetComponent<SpriteRenderer>().sprite = game.checkMateImage;
                }
                if (cp.name == "black_king") {                    
                    game.checkImageBlack.GetComponent<SpriteRenderer>().enabled = true;
                    game.checkImageBlack.GetComponent<SpriteRenderer>().sprite = game.checkMateImage;
                }
            }
            else {
                if (game.currentPlayer == "black") {                    
                    game.checkImageBlack.GetComponent<SpriteRenderer>().sprite = game.checkImage;
                }
                else {
                    game.checkImageWhite.GetComponent<SpriteRenderer>().sprite = game.checkImage;
                }
            }               
        }
    }
    public void OnMouseUp() {
        Game game = FindObjectOfType<Game>();
        game.checkImageWhite.GetComponent<SpriteRenderer>().enabled = false;
        game.checkImageBlack.GetComponent<SpriteRenderer>().enabled = false;

        controller = GameObject.FindGameObjectWithTag("GameController");

        Game newRook = FindObjectOfType<Game>();
        //Destroy the victim Chesspiece
        if (attack) {
            GameObject cp = controller.GetComponent<Game>().GetPosition(matrixX, matrixY);

            if (cp.name == "white_king") {
                controller.GetComponent<Game>().Winner("black");
                gameover = true;
            }
            if (cp.name == "black_king") {
                controller.GetComponent<Game>().Winner("white");
                gameover = true;
            }

            Destroy(cp);            
        }  
        
            
        //Set the Chesspiece's original location to be empty
        controller.GetComponent<Game>().SetPositionEmpty(reference.GetComponent<Chessman>().GetXBoard(),
            reference.GetComponent<Chessman>().GetYBoard());

        
        //Move reference chess piece to this position
        if (!castling) {
            reference.GetComponent<Chessman>().SetXBoard(matrixX);            
        }
        else {
            GameObject cp = controller.GetComponent<Game>().GetPosition(matrixX, matrixY);
            int xCp = cp.GetComponent<Chessman>().GetXBoard();            

            if (cp.name == "white_rook") {
                
                if (xCp == 9) {
                    reference.GetComponent<Chessman>().SetXBoard(matrixX - 2);
                    
                    Destroy(cp);
                    GameObject nR = newRook.Create("white_rook", 6, 0);
                    controller.GetComponent<Game>().SetPosition(nR);

                }
                if (xCp == 0) {
                    reference.GetComponent<Chessman>().SetXBoard(matrixX + 3);
                    
                    Destroy(cp);
                    GameObject nR = newRook.Create("white_rook", 4, 0);
                    controller.GetComponent<Game>().SetPosition(nR);
                }
            }
            if (cp.name == "black_rook") {
               
                if (xCp == 9) {
                    reference.GetComponent<Chessman>().SetXBoard(matrixX - 2);
                    
                    Destroy(cp);
                    GameObject nR = newRook.Create("black_rook", 6, 9);
                    controller.GetComponent<Game>().SetPosition(nR);
                }
                if (xCp == 0) {
                    reference.GetComponent<Chessman>().SetXBoard(matrixX + 3);
                    
                    Destroy(cp);
                    GameObject nR = newRook.Create("black_rook", 4, 9);
                    controller.GetComponent<Game>().SetPosition(nR);
                }
            }
        }   
        
        reference.GetComponent<Chessman>().SetYBoard(matrixY);
        reference.GetComponent<Chessman>().SetCoords();
        //Update the matrix
        controller.GetComponent<Game>().SetPosition(reference);

        ChangePawn();

        //Switch Current Player.........................................................................................................
        controller.GetComponent<Game>().NextTurn();

        //Destroy the move plates including self
        reference.GetComponent<Chessman>().DestroyMovePlates();

        FindObjectOfType<Game>().CheckOutKing();

        FindObjectOfType<Game>().CheckOutKingMate();  

        reference.GetComponent<Chessman>().EnableMovePlates();
    }
    private void ChangePawn() {
        GameObject cp = controller.GetComponent<Game>().GetPosition(matrixX, matrixY);
        FindObjectOfType<Game>().pawn = cp;
        int yCp = cp.GetComponent<Chessman>().GetYBoard();
        int xCp = cp.GetComponent<Chessman>().GetXBoard();
        // TODO: реализовать проверку на конец игры
        if (!gameover) {
            if (cp.name == "white_pawn" || cp.name == "white_pawn2") {
                if (yCp == 9) {
                    Game game = FindObjectOfType<Game>();
                    game.SetVisibleWhiteChangesPlate();
                    game.pawnXposition = xCp;
                    game.pawnYposition = yCp;
                }
            }
            if (cp.name == "black_pawn" || cp.name == "black_pawn2") {
                if (yCp == 0) {
                    Game game = FindObjectOfType<Game>();
                    game.SetVisibleBlackChangesPlate();
                    game.pawnXposition = xCp;
                    game.pawnYposition = yCp;
                }
            }
        }        
    }    
   
    public void SetCoords(int x, int y) {
        matrixX = x;
        matrixY = y;
    }

    public void SetReference(GameObject obj) {
        reference = obj;
    }

    public GameObject GetReference() {
        return reference;
    }
}
