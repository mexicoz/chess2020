﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : NetworkBehaviour
{
    [SerializeField] private float speed = 0.1f;
    
    void Update()
    {
        if (!isLocalPlayer) return;

        if (Input.GetKey(KeyCode.A))
            transform.Translate(-speed, 0, 0);
        if (Input.GetKey(KeyCode.D))
            transform.Translate(speed, 0, 0);
    }
    public override void OnStartLocalPlayer() {
        GetComponent<SpriteRenderer>().color = Color.red;
        base.OnStartLocalPlayer();
    }
}
